const logger = require('./app/logger/logger')
const startSocketServer = require('./app/server/websocketServer')
const startHttpsServer = require('./app/server/httpsServer')
const db_client = require('./app/db/config')
// require('./app/utils/memoryUsage')


logger.info('Starting...');

const server = startHttpsServer()

server.listen(process.env.WEB_PORT, '', () => {
    logger.info(`Server listening on port: ${process.env.WEB_PORT}`)
})

const socket_server = startSocketServer(server)

process.on('uncaughtException', (err) => {
    logger.error(err)
    process.exit(1)
})

process.on('SIGINT', async () => {
    logger.info('Process SIGINT. Shutting down...')
    server.close()
    await socket_server.close()
    await db_client.end()
    process.exit(0)
    
})
process.on('SIGTERM', async () => {
    logger.info('Process SIGTERM. Shutting down...')
    server.close()
    await socket_server.close()
    await db_client.end()
    process.exit(0)
})



