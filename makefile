# the image is contains in gitlab container registry
image_name := registry.gitlab.com/m11259/api
sources := yarn.lock package.json app/

.PHONY: all build build-image push-image

all: build-image push-image

build-image: $(sources)
	docker build -t $(image_name) .

push-image:
	docker push $(image_name)
