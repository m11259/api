FROM node:17

RUN node --version

WORKDIR /app

COPY package.json ./
COPY yarn.lock .

RUN yarn install

COPY . .

CMD [ "node", "index.js" ]
