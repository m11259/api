// Test for cite api: 
// 0) login as admin and check token
// 1) Make 8 guards, 2 with mistakes
// 2) update guards, delete guards
// 3) create sections
// 4) login as guard and check token
// 5) get section, change section
// 6) make users in db
// 8) establish ws connection
// 9) accept users, deny users, get all users, activate users
// 10) make reqs id db
// 11) accept reqs, deny reqs

const http = require('http')
const link = `http://localhost/api`
const WebSocket = require('ws')
const {Pool} = require('pg')
const getResponse = callback => resp => {
    const buffer = [];
    resp.on('data', chunk => {
        buffer.push(chunk)
	}).on('end', async () => {
		const body = buffer.join('')
		callback(JSON.parse(body), resp)
	})
}

const errHandler = (endpoint, method) => err => `Error at ${endpoint}, method: ${method}:\n${err}`

const log = console.log
const dir = console.dir


const client = {
	get: (endpoint, headers) => new Promise(resolve => http.request(`${link}${endpoint}`, {method: 'GET', headers: headers}, getResponse(resolve))
		.end()),
	post: (endpoint, body, headers) => new Promise(resolve => {
		const req = http.request(`${link}${endpoint}`, {method: 'POST', headers}, getResponse(resolve))
		req.write(JSON.stringify(body))
		req.end()
	}),
	put: (endpoint, body, headers) => endpoint.split('?')[1] ? 
		  new Promise(resolve => http.request(`${link}${endpoint}`, {method: 'PUT', headers}, getResponse(resolve))
			  .end()) 
		: new Promise(resolve => {
			const req = http.request(`${link}${endpoint}`, {method: 'PUT', headers}, getResponse(resolve))
			req.write(JSON.stringify(body))
			req.end()
		}),
	delete: (endpoint, headers) => new Promise(resolve => http.request(`${link}${endpoint}`, {method: 'DELETE', headers}, getResponse(resolve))
		.end()),
	patch: (endpoint, headers) => new Promise(resolve => http.request(`${link}${endpoint}`, {method: 'PATCH', headers}, getResponse(resolve)).end()),

};

function wsClient(ws) {
	ws.on("error", (err) => console.log(err))
	this.send = (data, token) => ws.send(JSON.stringify({data, token}))
	this.startSession = (token) => ws.on("open", () => this.send("auth", token))
	this.closeSession = () => ws.send({data: "close"})
	this.onMessage = callback => ws.on("message", msg => {
		const {data, type} = JSON.parse(msg.toString())
		if(type == "info"){
			console.log(data)
			return
		}
		callback(data, type)
	})
}

async function TestWebSocketApi(ws, db, admin_token, g_token) {
	let reqs_list = []
	const DataHandler = {
		users: (data) => console.dir({"updated_users": data}, {depth:5}),
		requests: (data) => console.dir({"updated_reqs": data}, {depth: 5}),
		lastreqs: (data) => console.dir({"updated_lastreqs": data}, {depth:5}),
		identsCounter: (data) => console.dir({"updated_idents": data}, {depth:5}),
		reqHistory: (data) => console.dir({"updated_reqHistory": data}, {depth:5}),
	}

	const messageHandler = (data, type) => {	
		console.log(type)
		type = type.split("/")[0]
		DataHandler[type](data)
		return
	}
	
	ws.startSession(admin_token)
	ws.onMessage(messageHandler)
	
	await db.MakeIdentifications({
		name: "Joseph",
		username: "Stalin",
		telegramname: "ToGoolag",
		quantity: 10
	})
	await db.MakeRequests(5)
	await new Promise(r => setTimeout(r, 500))
	ws.send("requests?0", admin_token)
	// await client.put(`/requests?680`, null, {token: admin_token})
	// await client.put(`/requests?681`, null, {token: admin_token})
	// await client.delete(`/requests?682`, {token: admin_token})
	// ws.send("reqHistory?0", admin_token)	
	// await db.Clear()
}

function DataBase(credentials){
	const pool = new Pool(credentials)
	this.MakeIdentifications = ({name, username, telegramname, quantity}) => Promise.all(
		[...Array(quantity).keys()].map(async id => {
			await pool.query(`insert into users(id, username, telegramname, firstname, phone, address) 
				values($1, $2, $3, $4, $5, $6)
				on conflict do nothing`, 
				[id, `${username + id}`, `${telegramname+id}`, `${name+id}`, `+3809292${id}`, (id*2+ 100).toString()])
			await pool.query('select pg_notify($1, $2)', ['user_created', id])
		}))
		
	this.MakeRequests = (quantity) => Promise.all(
		[...Array(quantity).keys()].map(async id => {
			const {rows} = await pool.query('insert into requests(user_id, body, user_address) values($1, $2, $3) on conflict do nothing returning *',
			[id, `laaaaaaa${id}`, id*5])
			await pool.query('select pg_notify($1, $2)', ['req_create', rows[0].id])
		})
	)
	this.Clear = async () => {
		await pool.query(`delete from requests`)
		await pool.query(`delete from users`)
	}
}

async function TestHttpApi(admin_token, g_token) {
	await client.post('/users/find', {
		address: 10
	}, {token: admin_token}).then(res => console.dir({res}))
}


(async () => {

	const {token} = await client.post('/admin/sign', {
		username: 'admin',
		password: 'PivoTeteriv3124'
	})
	const admin_token = token

	console.dir({admin_token})

	
	await client.post('/guards', {
		username: "g1",
		password: "1234"
	}, {token: admin_token})

	const tok = await client.post('/guards/sign', {
		username: "g1",
		password: "1234"
	})

	const g_token = tok.token
	dir({g_token})	
	const db = new DataBase({
		user: 'postgres',
		host: 'localhost',
		database: 'postgres',
		password: 'postgres',
		port: 5442
	})	
	const ws = new wsClient(new WebSocket('ws://localhost/ws/'))
	
	await TestWebSocketApi(ws, db, admin_token, g_token)

	// await TestHttpApi(admin_token, g_token)


	// const checked_adm_token = await client.get('/admin/sign', {token}).catch(errHandler('/admin/sign', 'GET'))
	// console.dir({checked_adm_token});
	
	// console.log('Creating guards...')

	// await Promise.all([...Array(8).keys()].map(async id => await client.post('/guards', {
	// 	username: `Guard${id}`,
	// 	password: "pivoteteriv"
	// }, {token}).then(data => console.log(data))))
	
	// const guards_list = await client.get('/guards?0', {token})

	// console.dir({guards_list})

	// console.log(`Creating guards mistakenly...`)
	
	// await client.post('/guards', {
	// 	username: 'GUard',
	// 	password: 'kek'
	// }, {token: "dsds"}).then(data => console.log(`On wrong token: ${data}`))

	// await client.post('/guards', {
	// 	username: 'Guard2',
	// 	password: 'kek'
	// }, {token}).then(data => console.log(`On user already exist: ${data}`))


	// console.log('Updating Guard0')
	
	// await client.put('/guards', {
	// 	id: guards_list.find(g => g.username == 'Guard0').id,
	// 	username: 'Kolbaska',
	// 	password: 'kek'
	// }, {token}).then(data => dir({update_data: data}))

	// console.log('Deleting Guard1')
	// await client.delete('/guards?7', {token}).then(data => log(data))

	
	// log('Creating sections')

	// await client.post('/sections', {
	// 	name: 'A',
	// 	from: 1,
	// 	to: 10
	// }, {token})

	// await client.post('/sections', {
	// 	name: 'B',
	// 	from: 11,
	// 	to: 20
	// }, {token})

	// await client.post('/sections', {
	// 	name: 'A',
	// 	from: 21,
	// 	to: 30
	// }, {token}).then(data => log(data == 'This section already exists'))


	// await client.post('/sections', {
	// 	name: 'C',
	// 	from: 1,
	// 	to: 100
	// }, {token})


	// let sections = await client.get('/sections', {token})
	
	// await client.put('/sections', {
	// 	id: sections.find(s => s.name == 'C').id,
	// 	flats_range: [21, 30]
	// }, {token}).then(data => log(data))

	// sections = await client.get('/sections', {token})

	// dir(sections)


	// log('Logging as Kolbaska')

	// token = await client.post('/guards/sign', {
	// 	username: 'Kolbaska',
	// 	password: 'kek'
	// })


	// dir(token)

	// token = token.token
	// let admin_token = await client.post('/admin/sign', {
	// 	username: 'admin',
	// 	password: 'PivoTeteriv3124'
	// })
	// admin_token = admin_token.token

	// const g_token = await client.get('/guards/sign', {token})

	// dir(g_token)
	
	// log('Connecting to socket server..')

	// const ws = new WebSocket('ws://localhost/ws/')

	// let users_list = []
	// let reqs_list = []
	// let lastreqs = []

	// ws.on('message', msg => {
	// 	if(msg == 'users/page?') {
	// 		ws.send(JSON.stringify({
	// 			data: 'users?0',
	// 			token: admin_token
	// 		}))
	// 	}else if(msg == 'requests/page?'){
	// 		ws.send(JSON.stringify({
	// 			data: 'requests?0',
	// 			token
	// 		}))
	// 	} else if(msg == 'lastreqs'){
	// 		ws.send(JSON.stringify({
	// 			data: 'lastreqs',
	// 			token
	// 		}))
	// 	}else {
	// 		msg = JSON.parse(msg)
	// 		if(msg.type == 'users') users_list = msg.data
	// 		if(msg.type == 'requests') reqs_list = msg.data
	// 		if(msg.type == 'lastreqs') lastreqs = msg.data
	// 	}
	// })
	
	// log('Creating users...')



	
	// await pool.query('delete from requests')
	// await pool.query('delete from users')

	// await Promise.all([...Array(30).keys()].map(async id => {
	// 	await pool.query(`insert into users(id, username, telegramname, firstname, phone, address) 
	// 		values($1, $2, $3, $4, $5, $6)
	// 		on conflict do nothing`, 
	// 		[id, `truecomm${id}`, `Joseph${id}`, `Stalin${id}`, `+3809292${id}`, (id*2+ 100).toString()])
	// 	await pool.query('NOTIFY user_update')
	// }))
	
	// await new Promise(resolve => setTimeout(resolve, 500))
	
	// dir({users_list})

	// await client.put(`/guards/section?${sections.find(x => x.name == 'A').id}`, null, {token}).then(data => log(data))

	// await client.get('/users?0', {token}).then(data => dir({users: data}))

	// await client.put('/users?1', null, {token})

	// await client.delete('/users?15', {token})

	// await client.put('/users/activation?1', null, {token})
	
	// await client.put('/users/activation?3', null, {token})
	// await client.patch('/users/activation?3', {token})

	// await client.get('/users/all?0', {token}).then(data => log(data))

	// await Promise.all([...Array(10).keys()].map(async id => {
	// 	await pool.query('insert into requests(user_id, body, user_address) values($1, $2, $3) on conflict do nothing',
	// 	[id, `laaaaaaa${id}`, id*5])
	// 	await pool.query('NOTIFY req_create')
	// }))

	// await new Promise(resolve => setTimeout(resolve, 500))

	// log(reqs_list)

	// client.put(`/requests?${reqs_list.find(x => x.user.user_id == 1).id}`, null, {token})

	// client.delete(`/requests?${reqs_list.find(x => x.user.user_id == 2).id}`, {token})
	
	// client.post('/requests', {
	// 	address: 50,
	// 	body: 'keeeeeeeeek'
	// }, {token}).then(data => log(data))



	// await new Promise(resolve => setTimeout(resolve, 500))
	
	

	// log(reqs_list)
	// dir({lastreqs}, {depth: 4})

	// client.post('/address/exists', {address: 101}, {token}).then(answer => log(answer))
	// client.post('/address/payments', {address: 101}, {token}).then(answer => log(answer))
})()


