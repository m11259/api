const logger = require('../logger/logger')
const fs = require('fs')
const path = require('path')
const generateAPI = require("../utils/generateAPI") 

const parse = body => {
    try{
        if(body){
            return JSON.parse(body)
        }
        return null
    } catch(e) {
        let error = new Error()
        error.message = "Error in JSON body"
        error.status = 400
        throw error
    }
}

const getBody = req => new Promise((resolve) => {
    const buffer = [];
    req.on('data', chunk => {
        buffer.push(chunk)
    }).on('end', async () => {
        const body = buffer.join('')
        resolve(body)
    })
})

const getHeaders = req => {
    const obj = {}
    for (let i = 0; i < req.rawHeaders.length;  i = i+2) {
        obj[req.rawHeaders[i]] = req.rawHeaders[i+1]
    }
    return obj
}

const getUrlInfo = url => {
	let args = url.split('?')[1]
    if (!Object.is(+args, NaN)) {
        args = +args
    }
	const link = url.split('?')[0].split('/').splice(2)
	return [link[0], link[1], args]
}

const api = {}
generateAPI(api, {
	pathFromIndex: process.cwd() + '/app/api/',
	pathToModules: 'api/endpoints/',
})

module.exports = async (req, res) => {
    const {method, url} = req
	let [endpoint, funcname, params] = getUrlInfo(url)
	if(params == undefined) params = parse(await getBody(req))
	// else params = +params
	const headers = getHeaders(req)
    try {
		const handler = api[endpoint][funcname] || api[endpoint]
        const answer = await handler[method](params, headers)
        res.statusCode = answer.status
        res.end(JSON.stringify(answer.body))
    } catch (err) {
		if(!api[endpoint] || !api[endpoint][method]){
			res.statusCode = '404'
			const message = 'Not found'
			logger.warn(`Client tried url ${url} \nresult:${e.stack}`)
			res.end(`${res.statusCode}: ${message}`)
		} else {
			res.statusCode = '500'
            logger.error(err)
        	const message = 'Internal Server Error' 
			res.end(`${res.statusCode}: ${message}`)
		}
    } finally {
        return [req, res]
    }
    
}
