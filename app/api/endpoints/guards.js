const db = require('../../db/db');
const crypto = require('../../utils/crypto_ware');
const {authAdmin, authGuard} = require('../../utils/auth_ware');
const {httpAnswer} = require('../../utils/utils_ware')

module.exports = {
    "POST": async (credentials, headers) => authAdmin(headers).then(async auth => {
        if (!auth) return httpAnswer(`Error, you doesn't have permission for this action`, 409)
        if(!(credentials.username && credentials.password)){
            return httpAnswer(`Error, wrong request body`, 400)
        }
        const hash_password = await crypto.hashPassword(credentials.password)
        const token = await crypto.generateToken()
        credentials = Object.assign(credentials, {hash_password, token})
		return db.Guard({create: credentials})
            .then(data => {
                if (data == undefined) {
                    return httpAnswer(`Your password or login is empty`, 400)
                } 
                else {
                    return httpAnswer(data, 201)
                }
            })
            .catch(() => httpAnswer('Error, this username already exists', 400))
    }),

    "GET": (page, headers) => authAdmin(headers).then(async authed => {
        if (!authed) return httpAnswer(`Error, you doesn't have permission for this action`, 409)
		return db.Guard({get: {page}}).then(data => httpAnswer(data, 200))
    }),

    "PUT": async (credentials, headers) => authAdmin(headers).then(async authed => {
       	const id = credentials.id
		if(!id || !(id > 0)) return httpAnswer('Error, wrong request body', 400)
        if (!authed) return httpAnswer(`Error, you doesn't have permission for this action`, 409)   
		const user = await db.Guard({getById: credentials.id})
        if(!user) return httpAnswer(`Error, this user does not exist`, 400)
        if (credentials.password) credentials.password = await crypto.hashPassword(credentials.password)
		return db.Guard({update: credentials}).then(data => httpAnswer(data, 200))
			.catch(data => httpAnswer('User already exists', 400))
    }),


    "DELETE": async (id, headers) =>  authAdmin(headers).then(authed => {
		if(!id || !(id > 0)) return httpAnswer('Error, wrong request params', 400)
	   if (!authed) return httpAnswer(`Error, you doesn't have permission for this action`, 409)
		else return db.Guard({delete: id}).then(data => httpAnswer(data, 200))
    }),

	section: {
		"PUT": async (section_id, {token}) => authGuard({token}).then(authed => {
			if(!section_id || !(section_id > 0)) return httpAnswer('Error, wrong request params', 400)
        	if (!authed) return httpAnswer(`Error, you doesn't have permission for this action`, 409)
			return db.Guard({changeSection: {section_id, token}}).then(data => httpAnswer(data, 200))
		}),
		
		"GET": async(_, {token}) => authGuard({token}).then(authed => {
        	if (!authed) return httpAnswer(`Error, you doesn't have permission for this action`, 409)
			return db.Guard({getSection: token}).then(data => httpAnswer(data, 200))

		})
	},
   	
	sign: {

    	"POST": async ({username, password}) => {
			const user = await db.Guard({getByUsername: username})
        	if(!user){
            	return httpAnswer('Error, this username does not exist', 400)
        	}
        	if(await crypto.verifyPassword(password, user.password)){
				const token = await db.Guard({retrieveToken: username})
            	return httpAnswer(token, 200)
        	} else {
            	return httpAnswer(`Authorization failed, invalid password or username`, 400)
        	}
    	},

		"GET": async (_, {token}) => token ? db.Guard({checkToken: token})
            	.then(res => (httpAnswer(res, 200)))
            	: httpAnswer(`Error, wrong request body, use object with field "token"`, 400),
	}
}
