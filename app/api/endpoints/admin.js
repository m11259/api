const db = require('../../db/db');
const crypto = require('../../utils/crypto_ware');
const {httpAnswer} = require('../../utils/utils_ware')


module.exports = {
   "sign":{
		 "POST": async ({username, password}) => {
			 const user = await db.Admin({get: {username}})
        if(!user){
            return httpAnswer('Error, this username does not exist', 400)
        }
        if(await crypto.verifyPassword(password, user.password)){
			const token = await db.Admin({retrieveToken: username})
            return httpAnswer(token, 200)
        } else {
                return httpAnswer(`Authorization failed, invalid password or username`, 400)
        }
    }, 


	   "GET": async (_, {token}) => token ? db.Admin({checkToken: token})
            .then((res) => {
				return httpAnswer(res, 200)
			})
            : httpAnswer(`Error, wrong request body, use object with field "token"`, 400)
	}
}
