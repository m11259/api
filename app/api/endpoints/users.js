const db = require('../../db/db');
const {httpAnswer, filterSectionAddress} = require('../../utils/utils_ware')
const {authGuard} = require('../../utils/auth_ware');

 

module.exports = {
    "GET": (page, headers) => authGuard(headers).then(async authed => {
        if (!authed) return httpAnswer(`Error, you doesn't have permission for this action`, 409)
		return db.Users({getUnidentified: {page}}).then(data => httpAnswer(data, 200))
    }),
   
    "PUT": (id, headers) => authGuard(headers).then(async authed => {
        if (!authed) return httpAnswer(`Error, you doesn't have permission for this action`, 409)
		if(!id || !(id > 0)) return httpAnswer('Error, wrong request params', 400)
		const user = await db.Users({getById: id})
        if(!user) return httpAnswer(`Error, user with this id does not exist`, 400)
		return db.Users({aprove: id}).then(data => httpAnswer(data, 200))
    }),
	
    "DELETE": (id, headers) => authGuard(headers).then(async authed => {
        if (!authed) return httpAnswer(`Error, you doesn't have permission for this action`, 409)
		if(!id || !(id > 0)) return httpAnswer('Error, wrong request params', 400)
		const user = await db.Users({getById: id})
        if(!user) return httpAnswer(`Error, user with this id does not exist`, 400)
		else return db.Users({deny: id}).then(data => httpAnswer(data, 200))
    }),

	all: {

		"GET": (page, headers) => authGuard(headers).then(async authed => {
			if (!authed) return httpAnswer(`Error, you doesn't have permission for this action`, 409)
			return db.Users({getAllUsers: {page}}).then(data => httpAnswer(data, 200))
    }),

	},

	activation: {

		 "PUT": (id, headers) => authGuard(headers).then(async authed => {
			if (!authed) return httpAnswer(`Error, you doesn't have permission for this action`, 409)
			if(!id || !(id > 0)) return httpAnswer('Error, wrong request params', 400)
			const user = await db.Users({getById: id})
			if(!user) return httpAnswer(`Error, user with this id does not exist`, 400)
			return await db.Users({activate: id}).then(data => httpAnswer(data, 200))
		}),

		"PATCH": (id, headers) => authGuard(headers).then(async authed => {
			if (!authed) return httpAnswer(`Error, you doesn't have permission for this action`, 409)
			if(!id || !(id > 0)) return httpAnswer('Error, wrong request params', 400)
			const user = await db.Users({getById: id})
			if(!user) return httpAnswer(`Error, user with this id does not exist`, 400)
			return await db.Users({deactivate: id}).then(data => httpAnswer(data, 200))
		})
	},

	find: {
		"POST": (body, headers) => authGuard(headers).then(async authed => {
			if (!authed) return httpAnswer(`Error, you doesn't have permission for this action`, 409)
			if(!(body.firstname || body.address || body.phone)) return httpAnswer("Wrong req params", 400)
			const page = body.page
			delete body.page
			return db.Users({findBy: {page, ...body}}).then(data => {
				return httpAnswer(data, 200)
			})
		}),
	},

	edit: {
			"PUT": (body, headers) => authGuard(headers).then(async authed => {
				if (!authed) return httpAnswer(`Error, you doesn't have permission for this action`, 409)
				if(!body.id) return httpAnswer("Wrong request params", 400)
				return db.Users({edit: body})
					.then(data => {
						return httpAnswer(data, 200)
					})
					.catch(({message}) => {
						if(message.split(" ").slice(-3).join(' ') == "does not exist"){
							return httpAnswer("Wrong request params", 400)
					}
				}) 
			})
		}
    
}
