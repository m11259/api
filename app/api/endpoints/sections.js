const db = require('../../db/db');
const {authAdmin, authGuard} = require('../../utils/auth_ware');
const {httpAnswer} = require('../../utils/utils_ware')

module.exports = { 
	"GET": (_, headers) => authGuard(headers).then(() => db.Sections({get: ''})
            .then(data => httpAnswer(data, 200))),
         
    "POST": ({name, from, to}, headers) => authAdmin(headers).then(async authed => {
        if (!authed) return httpAnswer(`Error, you doesn't have permission for this action`, 409)
        if(!(from > 0 || to > 0)) return httpAnswer('From and to must be natural numbers', 400) 
		return db.Sections({create: {name, from, to}}).then(section => {
            if(!section) return httpAnswer('This section already exists', 400)
            return httpAnswer(section, 201)
        })  
    }), 

    "PUT": ({id, name, flats_range}, headers) =>  authAdmin(headers).then(async authed => {
        if(!id || !(id > 0) ) return httpAnswer('Error, wrong request body', 400)
        if (!authed) return httpAnswer(`Error, you doesn't have permission for this action`, 409)
		return db.Sections({update: {id, name, flats_range}}).then(section => httpAnswer(section, 201))  
    }), 
         
    "DELETE": (id, headers) => authAdmin(headers).then(async authed => {
        if(!id || !(id > 0) ) return httpAnswer('Error, wrong request body', 400)
        if (!authed) return httpAnswer(`Error, you doesn't have permission for this action`, 409)
		return db.Sections({del: id}).then(() => httpAnswer(`Section with id ${id} deleted`, 200))
    }), 

}

