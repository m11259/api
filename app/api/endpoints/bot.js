const {authAdmin} = require("../../utils/auth_ware");
const {httpAnswer, parseSigmaKeys} = require("../../utils/utils_ware")
const redis = require('../../db/redis')

module.exports = {
	"PUT": ({key, data}, headers) => authAdmin(headers).then(async authed => {
		if (!authed) return httpAnswer(`Error, you doesn't have permission for this action`, 409)
		const key_exists = await redis.keyExists(key)
		if (!key_exists) {
			return httpAnswer(`Error, wrong request parameters`, 400)
		}
		await redis.set(key, data)
		return httpAnswer("OK", 200)
	}),

	"GET": (key, headers) => authAdmin(headers).then(async authed => {
		if (!authed) return httpAnswer(`Error, you doesn't have permission for this action`, 409)
        const value = await redis.get(key)
		return value ?
			httpAnswer(value, 200) :
			httpAnswer("Error, wrong request parameters", 400)
	}),

    keys: {
       "GET": (_, headers) => authAdmin(headers).then(async authed => {
            if (!authed) return httpAnswer(`Error, you doesn't have permission for this action`, 409)
            const raw = await redis.getKeys('en*')
            const sigma_keys = parseSigmaKeys(raw)
            return httpAnswer(sigma_keys, 200)
       })
    }
}
