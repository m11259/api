const {hasClientEnoughPayment, addressExists} = require('../../db/redis')
const {authGuard} = require('../../utils/auth_ware')
const {httpAnswer} = require('../../utils/utils_ware')

module.exports = {
	payments: {
		'POST': ({address}, headers) => authGuard(headers).then(async authed => {
			if(!address) return httpAnswer('Error, wrong request body', 400)
			if(!(await addressExists(address))) return httpAnswer("This address doesn't exists", 400)
			const answer = await hasClientEnoughPayment(address)
			return httpAnswer(answer, 200)
		})
	},
}

