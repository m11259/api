const db = require('../../db/db')
const redis = require('../../db/redis')
const {authGuard} = require('../../utils/auth_ware');
const {httpAnswer, json2csv, filterSectionAddress} = require('../../utils/utils_ware')
const {writeFile} = require('fs').promises
const logger = require('../../logger/logger')

module.exports = {
	file: {
		'POST': ({from, to}, headers) => authGuard(headers).then(async authed => {
			if (!authed) return httpAnswer(`Error, you doesn't have permission for this action`, 409)
			if(new Date(from) == 'Invalid Date' || new Date(to) == 'Invalid Date') return httpAnswer("Error, 'from' and 'to' are type of 'Date'", 400)
			let requests_data = await db.Requests({getHistory: {from, to}})
			requests_data = json2csv(requests_data)
			const from_to_string = [from, to].map(x => new Date(x).toLocaleString('uk-UK').replace(', ',';')).join('-')
			await writeFile(`./tables/${from_to_string}.csv`, requests_data)
			return httpAnswer(`tables/${from_to_string}.csv`, 200)
		})

	},

	dismiss: {
		'PUT': (id, headers) => authGuard(headers).then(async authed => {
			if(!id || !(id > 0)) return httpAnswer('Error, wrong request params', 400)
        	if (!authed) return httpAnswer(`Error, you doesn't have permission for this action`, 409)
			await db.Requests({unAccept: id})
			return httpAnswer(`Request ${id} set unaccepted`, 200) 
		})
	},

	edit: {
		'PUT': ({id, address, body, car_number}, headers) => authGuard(headers).then(async authed => {
			if(!id || !(id > 0) || !pass.type || !pass.id) return httpAnswer('Error, wrong request params', 400)
        	if (!authed) return httpAnswer(`Error, you doesn't have permission for this action`, 409)
			await db.Requests({edit: {id, address, body, car_number}})
			return httpAnswer(`Request ${id} edited`, 200)
		})
	},

    // fulfill: {
    //     'PUT': (id, headers) => authGuard(headers).then(async authed => {
    //         if(!id || !(id > 0)) return httpAnswer('Error, wrong request params', 400)
    //         if (!authed) return httpAnswer(`Error, you doesn't have permission for this action`, 409)
    //         await db.Requests({fulfill: id})
    //         return httpAnswer(`Fulfilled request with id ${id}`, 200)
    //     }),
    // },

    setPending: {
        'PUT': ({id, pass}, headers) => authGuard(headers).then(async authed => {
            pass = JSON.parse(pass)
			if(!id || !pass.type || !pass.id) return httpAnswer('Error, wrong request params', 400)
        	if (!authed) return httpAnswer(`Error, you doesn't have permission for this action`, 409)
            const answer = await db.Requests({edit: {id, pass, reviewed_at: true}})
            answer.pass = pass
            return httpAnswer(answer, 200)
        }),
    },

	'POST': ({address, body, car_number}, headers) => authGuard(headers).then(async authed => {
		if(!address && !body) return httpAnswer('Error, wrong request params', 400)
        if (!authed) return httpAnswer(`Error, you doesn't have permission for this action`, 409)
		let answer
		try{	
			answer = await db.Requests({create: {address, body, car_number}})
			redis.pushNewRequest(answer)
		} catch(e) {
			logger.warn(e)
			if(e.message.includes('insert or update on table "requests" violates foreign key constraint')){
				return httpAnswer(`Error, this user does not exist`, 400)
			}	
		}
		return httpAnswer(answer, 201)
	}),

    'PUT': (id, headers) => authGuard(headers).then(async authed => {
		if(!id || !(id > 0)) return httpAnswer('Error, wrong request params', 400)
        if (!authed) return httpAnswer(`Error, you doesn't have permission for this action`, 409)
        const { pass } = await db.Requests({getById: id})
        console.log(pass)
        pass === null ? await db.Requests({accept: id}) : await db.Requests({fulfill: id})
        return httpAnswer(`Aprooved request with id ${id}`, 200)
    }),

    'DELETE': (id, headers) => authGuard(headers).then(async authed => {
		if(!id || !(id > 0)) return httpAnswer('Error, wrong request params', 400)
        if (!authed) return httpAnswer(`Error, you doesn't have permission for this action`, 409)
		await db.Requests({deny: id})
        return httpAnswer(`Deleted request with id ${id}`, 200)
    }),
}
