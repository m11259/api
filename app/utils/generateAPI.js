const fs = require('fs')
const path = require('path')
const logger = require('../logger/logger')
module.exports = (api, {pathFromIndex, pathToModules, onUpdate}) => {
	pathToModules = "../" + pathToModules
	const cacheFile = (name) => {
		const filePath = pathToModules + name;
		const key = path.basename(filePath, '.js');
		try {
			const libPath = require.resolve(filePath);
			delete require.cache[libPath];
		} catch (e) {
			logger.error(e)
			return;
		}
		try {
			const method = require(filePath);
			api[key] = onUpdate ? onUpdate(api, method) : method;
		} catch (e) {
			logger.error(e)
			delete api[key];
		}
	}

	const cacheFolder = (path) => {
		fs.readdir(pathFromIndex + pathToModules, (err, files) => {
			if (err){
				logger.error(err)
					return;
			}
			files.forEach(cacheFile);
		});
	};

	const watch = (path) => {
		fs.watch(pathFromIndex + path, (event, file) => {
			cacheFile(file);
			logger.info(`Succesfully updated ${file}`)
		});
	};
	cacheFolder(pathToModules);
	watch(pathToModules);
}
