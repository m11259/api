//const Crypto  = require('crypto')
const {generateToken, hashPassword, validatePassword} = require('metautil')

const secret = process.env.SECRET
const chars = 'qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890'
const length = 16

module.exports = {
    generateToken: () => generateToken(secret, chars, length),

	hashPassword: hashPassword,

	verifyPassword: validatePassword

//    hashPassword: (password) => new Promise(resolve => {
//        const salt = Crypto.randomBytes(16).toString("hex")
//        Crypto.scrypt(password, salt, 16, (err, derivedKey) => {
//            if (err) reject(err);
//            resolve(salt + ":" + derivedKey.toString('hex'))
//        })
//    }),
//
//    verifyPassword: (password, hash) => new Promise(resolve => {
//        const [salt, key] = hash.split(":")
//        Crypto.scrypt(password, salt, 16, (err, derivedKey) => {
//            if (err) reject(err);
//            resolve(key == derivedKey.toString('hex'))
//        });
//    })
}
