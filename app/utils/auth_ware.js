const db = require('../db/db');

module.exports = {
    
	authAdmin: ({token}) => db.Admin({checkToken: token}),

    authGuard: ({token}) => new Promise(resolve => {
		db.Admin({checkToken: token}).then(authedAsAdmin => {
            if(authedAsAdmin) resolve(true)
		}).then(() => db.Guard({checkToken: token}))
        .then(res => resolve(res))
    }),
}
