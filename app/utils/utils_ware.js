const db = require('../db/db');
const sigma_helper = require('./sigma_helper.json')



module.exports = {
    checkSection: name => db.Sections.get().then(listSecs => listSecs.some(x => x.name == name)),

    httpAnswer: (data, statusCode) => ({
        body: data,
        status: statusCode
    }),

    json2csv: table => {
        if(table.length == 0) return ''
        let csv_string_header = Object.keys(table[0]).join(',') + '\n'
		const date_fields = ['created_at', 'reviewed_at']
        let csv_string = table.map(column => {
            date_fields.forEach(fld => column[fld] = column[fld] ? column[fld].toLocaleString('uk-UK', {timeZone: 'Europe/Kiev'}) : '')
			return Object.values(column).map(val => {
				if(typeof val == 'boolean'){
					return val ? "Принята" : "Не принята"
				} else {
					return '"' + val + '"'
				}
			}).toString()
        })
        return csv_string_header + csv_string.join('\n')
    },

	prepareRequestsData: (data) => {
		const prepareRequest = keys => row => {
			if(!keys) keys = Object.keys(row)
			row.user = {}
			keys.forEach(key => {
				if(key != 'id' && key != 'created_at' && key != 'body' && key != 'car_number' && key != 'user_address' && key != 'reviewed_at' && key != 'accepted', key != 'fulfilled_at', key != 'pass'){
					row.user[key] = row[key]
					delete row[key]
				}
			})
			row.user['address'] = row['user_address']
			delete row['address']
			return row
		}
		if(data[0]){
			const keys = Object.keys(data[0])
			data = data.map(prepareRequest(keys))
		} else {
			data = prepareRequest()(data)
		}
		
		return data
	},

    parseSigmaKeys: (raw) => {
        const keysIterator = function*(keys) {
            const prev_keys = []
            for (const key of keys) {
                yield [prev_keys, key]
                prev_keys.push(key)
            }
        }
        const getDeep = (obj, path) => path.reduce((prev, node) => prev[node], obj)
        const setDeep = (obj, path, value) => {
            let par = obj
            while (path.length > 1) {
                node = path.shift()
                par = par[node]
            }
            par[path.shift()] = value
        }

        let res = {}
        for(const path_string of raw) {
            const keys = path_string.substring(3).split(":")
            if(keys[keys.length - 1] == "message") keys.pop()
            if(keys.some(key => ['keyboard', 'buttons'].includes(key))) {
                continue
            }
            for (const [prev_keys, key] of keysIterator(keys)) {
                if(!getDeep(res, [...prev_keys, key])){
                    setDeep(res, [...prev_keys, key], {})
                }
            }
            setDeep(res, keys, { 
                ...getDeep(sigma_helper, keys),
                url: path_string.substring(2)
            })
        }
        return res
    },

    unpackObject: (obj, cb) => {
        entries = Object.entries(obj)
        return entries.map(([key, value], i) => cb(key, value, i))
    }
}
