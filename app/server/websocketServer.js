const db = require('../db/db');
const logger = require('../logger/logger')
const WebSocket = require('ws')
const {sessionHandler, apiHandler, sendHandler} = require('../websocket/websocketHandler')
const websocketListeners = require('../websocket/websocketListeners')
// const wsApi = require('../wsApi/wsApi')
const {authGuard} = require('../utils/auth_ware');
const WsSessions = require('../websocket/wsSessions')

const timeDiff = start => ({
    seconds: process.hrtime()[0] - start[0],
    miliseconds: ((process.hrtime()[1] - start[1]) / 1000000).toFixed(2)
})

module.exports = async (server) => {
    try{
        const ws = new WebSocket.Server({server})
		
        logger.info(`Socket server listening on port: 3000`)
		const wsSessions = new WsSessions(ws)

		websocketListeners(sendHandler(wsSessions))

        ws.on('error', err => logger.error(err))
		ws.on('connection', (connection, req) => {
			logger.info(`WebSocket connected to ${req.socket.remoteAddress}:${req.socket.remotePort}`)
			
			connection.on("close", () => {
				wsSessions.closeSession(connection, req)
				logger.info(`Session closed with client ${req.socket.remoteAddress}:${req.socket.remotePort}`)
			})

			connection.on("message", async message => {
				if(sessionHandler(wsSessions, connection, req, message)){
					return
				}
				const time_start = process.hrtime()
				const endpoint = await apiHandler(connection, message)
				logger.responseInfo(
					'WebSocket message', 
					200, 
					endpoint, 
					req.socket.remoteAddress, 
					req.socket.remotePort, 
					timeDiff(time_start)
				)
			})
			
		})
		return ws

    }  catch (e) {
        logger.error(`Error starting websocket server, reason: ${e.stack}`)
    }
}


