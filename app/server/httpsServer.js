const https = require('https')
const http = require('http')
const logger = require('../logger/logger')
const router = require('../api/apiHandler')


const timeDiff = start => ({
    seconds: process.hrtime()[0] - start[0],
    miliseconds: ((process.hrtime()[1] - start[1]) / 1000000).toFixed(2)
})

const createServer = (options, callback) => {
    if(!options) {
        return http.createServer(callback)
    }   
    http.createServer((req, res) => {
        res.writeHead(301, { "Location": "https://" + req.headers['host'].replace(process.env.WEB_PORT, process.env.WEB_PORT) + req.url });
        res.end()
    }).listen(process.env.WEB_PORT)
    return https.createServer(options, callback)
}

module.exports = options => createServer(options, async (req, res) => {
        const time_start = process.hrtime()
        const handler = router 
        const [{url, method, socket}, {statusCode}] = await handler(req, res)
        logger.responseInfo(method, url, statusCode, socket.remoteAddress, socket.remotePort, timeDiff(time_start))  
})
