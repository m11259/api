const client = require('./config');
const credentials = require('./credentials.json')
const logger = require('../logger/logger')
const Emitter = new require('events')

const PAGINATION_MULTIPLE = 20;
const generateAPI = require("../utils/generateAPI") 

const createIsInSectionFunction = async () => {
	const sql = `create or replace function isInSection(address text, section_name text) returns boolean as $$
                   	declare
                    	declare flat integer;
                        flats_range integer[2];
                    begin
                        flat := cast(substring(address from '^[0-9]+') as integer);
                        flats_range := (select sections.flats_range from sections where section_name = sections.name);
                        return flat <= flats_range[2] and flat >= flats_range[1];
                    end;             
                    $$ LANGUAGE plpgsql;`
	await client.query(sql).then(() => logger.info(`Created sql function`))
}

const APIGeneratorSettings ={
	pathFromIndex: process.cwd() + '/app/db/',
	pathToModules: 'db/entities/',
	onUpdate: (api, createModule) => {
			const mod = createModule(client)
			if(mod.events) api.events = new Set([...api.events, ...mod.events]) 
			client.query(`UNLISTEN *`)
				.then(() => api.events.forEach(ev => {
					client.query(`LISTEN ${ev}`)
				}))
			return (dbReqParams) => {
				const [func, args] = Object.entries(dbReqParams)[0]
				return mod[func](args).then(res => {
					return res
				})
		}
	}
}
const db = {
	events: new Set()
} 
generateAPI(db, APIGeneratorSettings)
setTimeout(async () => {
	await db.Admin({create: credentials})
	await createIsInSectionFunction()
}, 1000)

const emitter = new Emitter()
client.on('notification', msg => emitter.emit(msg.channel, msg.payload))

db.on = (events, listener) => {
	for(let event of events){
		emitter.on(event, listener)
	}
},

module.exports = db
