const { promisify } = require("util")
const redis = require("redis");
const logger = require("../logger/logger")

const client = redis.createClient({
	url: "redis://" + process.env['WORKER_REDIS_ADDRESS'],
});

client.on("error", function(error) {
	logger.error(error);
});

const newRequestsKey = "new-requests"
const lastMonthPaymentsKey = "last-months-payments" 

module.exports = {
	async addressExists(address) {
		const ahexists = promisify(client.hexists).bind(client)
		const res = await ahexists(lastMonthPaymentsKey, address)
		return res
	},

	async hasClientEnoughPayment(address) {
		const minPaymentThreshold = 125
		const ahmget = promisify(client.hmget).bind(client)
		const res = await ahmget(lastMonthPaymentsKey, address)
		return res >= minPaymentThreshold
	},

	async pushNewRequest({body, user_address, created_at}){
		const alpush = promisify(client.lpush).bind(client)
		created_at = created_at.toLocaleString('de').replace(',', '')
		await alpush(newRequestsKey, [user_address, body, created_at].join(';'))
	},

	async get(key) {
		const aget = promisify(client.get).bind(client)
		const res = aget(key)
		return res
	},

    async getKeys(key) {
        const akeys = promisify(client.keys).bind(client)
        const keys = await akeys(key)
        return keys
    },
	
	async keyExists(key) {
		const value = await this.get(key)
		return !!value
	},

	async set(key, value) {
		const aset = promisify(client.set).bind(client)
		aset(key, value)
	}
}
