const crypto = require('../../utils/crypto_ware');

module.exports = (client) => ({
    get: ({username, token}) => {
        let query, params
		if (username) {
            query = `select * from Admins where username = $1;`
            params = [username]
        } else if (token) {
            query = `select * from Admins where token = $1;`
            params = [token]
        }
        return client.query(query, params).then((res) => res.rows[0])
    },

    create: async function ({username, password}) {
        if (username && password) {
            const query = `insert into admins(username, password, token) values ($1, $2, $3)
            on conflict do nothing`
            const hash_password = await crypto.hashPassword(password)
            const token = await crypto.generateToken()
            return client.query(query, [username, hash_password, token])
        }
        return undefined
    },

    checkToken: (token) => {
        const query = `SELECT ($1 IN (SELECT token from admins))`
        return client.query(query, [token]).then(({rows}) => rows[0]["?column?"])
    },

    retrieveToken: (username) => { 
        const query = `select token from admins where username=$1`
        return client.query(query, [username]).then(({rows}) => {
			return rows[0]
		})
    }
})
