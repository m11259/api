module.exports = (client) => ({
	get: () => {
		const sql = 'select * from sections'
		return client.query(sql).then(({rows}) => rows)
	},

	getByAddress: (address) => {
		const sql = `select sections.* from sections where isInSection($1, sections.name)`
		return client.query(sql, [address]).then(res => res.rows)
	},

	create: ({name, from, to}) => {
		const sql = `insert into sections(name, flats_range) values($1, $2) 
		on conflict do nothing
		returning *` 
		return client.query(sql, [name, [from, to]]).then(({rows}) => rows[0])
	},

	update: ({id, name, flats_range}) => {
		let query
		if(name && flats_range) {
			query = ['update sections set name=$1, flats_range=$2 WHERE id=$3 returning *', [name, flats_range, id]] 
		} else if(name) {
			query = ['update sections set name=$1 WHERE id=$2 returning *', [name, id]] 
		} else {
			query = ['update sections set flats_range=$1 WHERE id=$2 returning *', [flats_range, id]] 
		}
		return client.query(...query)
			.then(res => res.rows[0])
	},

	del: id => {
		const sql = `delete from sections where id=$1
		returning *`
		return client.query(sql, [id]).then(({rows}) => rows[0])
	}
})
