const PAGINATION_MULTIPLE = 20;
module.exports = (client) => ({
    getUnidentified: ({page, section}) => {
        if(page != null) {
			if(section){
    			const query =`
					select users.* from users join sections on sections.name = $3
					where identified=false and isInSection(users.address, sections.name) 
					order by updated_at desc LIMIT $1 OFFSET $2`
                	return client.query(query, [PAGINATION_MULTIPLE, page*PAGINATION_MULTIPLE, section]).then(res => res.rows)
			} else {
    			const query =`select * from users where identified=false order by updated_at desc LIMIT $1 OFFSET $2`
                	return client.query(query, [PAGINATION_MULTIPLE, page*PAGINATION_MULTIPLE,]).then(res => res.rows)

			}
        } else {
			if(section){
				const query =`
					select users.* from users join sections on sections.name = $1
					where identified=false and isInSection(users.address, sections.name) 
					order by updated_at desc`
                return client.query(query, [section]).then(res => res.rows)
	
			} else {
            	const query =`select * from users where identified=false order by updated_at desc`
                	return client.query(query).then(res => res.rows)
			}
        }
	},	

	getAllUsers: ({page, section}) => {
		if(page != null) {
			if(section){
            	const query =`select users.* from users join sections on sections.name = $3
						where isInSection(users.address, sections.name) 
						order by updated_at desc LIMIT $1 OFFSET $2`
                	return client.query(query, [PAGINATION_MULTIPLE*3, page*PAGINATION_MULTIPLE*3, section]).then(res => res.rows)
			} else {
				const query =`select * from users order by updated_at desc LIMIT $1 OFFSET $2`
					return client.query(query, [PAGINATION_MULTIPLE*3, page*PAGINATION_MULTIPLE*3]).then(res => {
						return res.rows
					})
			}
        } else {
			if(section){
				const query =`select users.* from users join sections on sections.name = $1
						where isInSection(users.address, sections.name) 
						order by updated_at desc` 
					return client.query(query, [section]).then(res => res.rows)
			} else {
				const query =`select * from users order by updated_at desc`
					return client.query(query).then(res => res.rows)
			}
        }
	},

	getIdentsCounter: () => {
		const query = "select count(*) from users where identified=false"
		return client.query(query).then(res => {
			return res.rows[0].count
		})
	},

    getById: (id) => {
        const query = `select * from users where id=$1`
        return client.query(query, [id])
            .then(res => {
				return res.rows[0]
			})
    },

	findBy: ({page, ...findParams}) => {
		if(findParams.address == "") delete findParams.address
		findParams = Object.entries(findParams)
		const queryParams = findParams.map(([key, param], id) => key != "address" ? `${key} like \'${param + '%'}\'` : `${key}=\'${param}\'`).join(' and ')
		const query = `select * from users where `
		return client.query(query + queryParams + ` LIMIT $1 OFFSET $2`, [PAGINATION_MULTIPLE*3, page*PAGINATION_MULTIPLE*3])
			.then(res => res.rows) 
	},

    aprove(id) {
        const query = `update users set identified=true where id=$1`
        return client.query(query, [id])
            .then(async () => {
                await client.query(`select pg_notify($1, $2)`, ['user_approved', id])
                return `User with id ${id} now approved`
            })
    },

    deny(id) {
        const query = `delete from users where id=$1`
        return client.query(query, [id])
            .then(async () => {
				await client.query(`select pg_notify($1, $2)`, ['user_denied', id])
                return `User with id ${id} now denied`
            })
    },

	activate: id => {
		const query = `update users set activated=true where id=$1`
		return client.query(query, [id])
            .then(async () => {
				await client.query('select pg_notify($1, $2)', ['user_activated', id])
                return `User with id ${id} now active`
            })
	
	},

	deactivate: id => {
		const query = `update users set activated=false where id=$1`
		return client.query(query, [id])
			.then(async () => {
				await client.query('select pg_notify($1, $2)', ['user_deactivated', id])
				return `User with id ${id} now deactivated`
			})

	},

	edit: ({id, ...credentials}) => {
		const query = `update users set `
		const querySetParams = Object.entries(credentials).map(([key, value], id) => `${key}=$${id+2}`).join(", ")
		const qeuryEnd = ` where id=$1 returning *`
		return client.query(query + querySetParams + qeuryEnd, [id, ...Object.values(credentials)])
			.then(({rows}) => {
				return rows[0]
			})
	},

	events: ['user_created', 'user_approved', 'user_denied', 'user_activated', 'user_deactivated']
})
