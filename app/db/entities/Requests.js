const {unpackObject} = require('../../utils/utils_ware')

const PAGINATION_MULTIPLE = 20;

module.exports = (client) => ({
	getHistory: ({from, to, page, type}) => {
			const query = `select requests.id, requests.body, case requests.accepted when 1 then true else false end as accepted, users.firstname, users.lastname, users.username, users.phone, requests.user_address, requests.created_at, requests.reviewed_at 
				from requests left join users on users.id = requests.user_id or requests.user_id = null\n`
			if(page != undefined){
				if(type == 'pages'){
					const query_page = `where requests.accepted != 0 order by reviewed_at desc LIMIT $1`
					return client.query(query + query_page, [(page+1)*PAGINATION_MULTIPLE*3]).then(res => {
						return res.rows
					})
				} else {
					const query_page = `where requests.accepted != 0 order by reviewed_at desc LIMIT $1 OFFSET $2`
					return client.query(query + query_page, [PAGINATION_MULTIPLE*3, page*PAGINATION_MULTIPLE*3]).then(res => {
						return res.rows
					})
				}
			} else {
				const query_fromTo = `\nwhere requests.accepted != 0 and requests.created_at >= $1 and requests.created_at <= $2 
				order by created_at desc`
				return client.query(query+query_fromTo, [from, to]).then(res => res.rows)
			}
    },

    
	getLastAccepted: ({number, section}) => {
		if(section){
			const query =`select users.*, requests.*
				from requests 
				left join users on requests.user_id=users.id
				join sections on sections.name = $2
				where accepted !=0 and isInSection(requests.user_address, sections.name) order by requests.reviewed_at desc LIMIT $1`
			return client.query(query, [number, section]).then(res => res.rows)

		} else {
			const query = `select users.*, requests.*
					from requests 
					left join users on requests.user_id=users.id
					where accepted !=0 order by requests.reviewed_at desc LIMIT $1`
			return client.query(query, [number]).then(res => res.rows)
		}
	},

	get: ({page, section}) => {
		if(page != null){
			if(section){
				const query =`select users.*, requests.*
                from requests 
            	left join users on requests.user_id=users.id
                join sections on sections.name = $3
                where accepted=0 and isInSection(requests.user_address, sections.name) order by requests.created_at desc LIMIT $1 OFFSET $2`
					return client.query(query, [PAGINATION_MULTIPLE*3, page*PAGINATION_MULTIPLE*3, section]).then(res => res.rows)
			} else {
				const query =`select users.*, requests.*
				from requests 
				left join users on requests.user_id=users.id
				where accepted=0 order by requests.created_at desc LIMIT $1 OFFSET $2`
					return client.query(query, [PAGINATION_MULTIPLE*3, page*PAGINATION_MULTIPLE*3]).then(res => res.rows)
			}
		} else{
			if(section){
				const query =`select users.*, requests.*
				from requests 
				left join users on requests.user_id=users.id
				join sections on sections.name = $1
				where accepted=0 and isInSection(requests.user_address, sections.name) order by requests.created_at desc`
					return client.query(query, [section]).then(res => res.rows)
			} else { 
				const query =`select users.*, requests.*
				from requests 
				left join users on requests.user_id=users.id
				where accepted=0 order by requests.created_at desc`
					return client.query(query).then(res => res.rows)
			}
		}
    },

	getPages: ({page, section}) => {
		if(section){
			const query =`select users.*, requests.*
			from requests 
			left join users on requests.user_id=users.id
			join sections on sections.name = $2
			where accepted=0 and isInSection(requests.user_address, sections.name) order by requests.created_at desc LIMIT $1`
				return client.query(query, [(page+1)*PAGINATION_MULTIPLE*3, section]).then(res => res.rows)
		} else {
			const query =`select users.*, requests.*
			from requests 
			left join users on requests.user_id=users.id
			where accepted=0 order by requests.created_at desc LIMIT $1`
				return client.query(query, [(page+1)*PAGINATION_MULTIPLE*3]).then(res => res.rows)
		}
	},

	getById: (id) => {
		const query = `select users.*, requests.*
				from requests 
				left join users on requests.user_id=users.id
				where requests.id=$1`
		return client.query(query, [id]).then(res => {
			return res.rows[0]
		})
	},

    accept: (id, fulfill) => {
        const query = `update requests set accepted=1, reviewed_at=now() where id=$1`
        console.log(query)
        return client.query(query, [id]).then(async res => {
			return await client.query(`select pg_notify($1, $2)`, ['req_accept', id])
		})
    },

    fulfill: (id) => {
        const query = `update requests set accepted=1, fulfilled_at=now() where id=$1`
        return client.query(query, [id]).then(async res => {
			return await client.query(`select pg_notify($1, $2)`, ['req_fulfill', id])
		})
    },

    deny: (id) => {
        const query = `update requests set accepted=-1, reviewed_at=now() where id=$1`
		return client.query(query, [id]).then(async data => {
			await client.query(`select pg_notify($1, $2)`, ['req_deny', id])
			return data
		})
    },

	create: ({address, body, car_number}) => {
		const query = car_number ? [`insert into requests(user_address, body, car_number) values($1,$2,$3) returning *`, [address, body, car_number]] : [`insert into requests(user_address, body) values($1,$2) returning *`, [address, body]]
		return client.query(...query)
			.then(({rows}) => rows[0])
			.then(({id, user_id, body, user_address, created_at}) => {
				client.query('select pg_notify($1, $2)', ['req_created_guard', id])
				return {id, user_id, body, user_address, created_at}
			})
	},
	
	unAccept: async (id) => {
		const query = `update requests x set accepted=0  from requests y where x.id=y.id and x.id=$1 returning y.accepted as prev`
		return client.query(query, [id]).then(async ({rows}) => {
			if(rows[0].prev == "1"){
				await client.query('select pg_notify($1, $2)', ['req_unaccept', id])
			} else {
				await client.query('select pg_notify($1, $2)', ['req_undeny', id])
			}
		})
	},

	edit: async ({id, ...params}) => {
        const values = []
        const set_query = unpackObject(params, (key, value) => {
            switch(key){
                case 'pass':
                    return `accepted=2, ${key}=ROW(${unpackObject(value, (_, v) => `'${v}'`).join(',')})`
                case 'reviewed_at':
                case 'fulfilled_at':
                    return `${key}=now()`
                default:
                    return `${key}=${value}`
            }
        }).join(', ')
		const query = `update requests set ${set_query} 
			where id=${id}
            returning *`
		const res = await client.query(query, values)
		await client.query(`select pg_notify($1, $2)`, ['req_update', id])
        return res.rows[0]
	},


	events: ['req_create', 'req_created_guard', 'req_cancel', 'req_accept', 'req_unaccept', 'req_deny', 'req_undeny', 'req_update']
})
