const PAGINATION_MULTIPLE = 20;
module.exports = (client) => ({
    get: ({page}) => {
        if(page != null) {
            const sql =`select id, username, created_at from guards ORDER BY created_at DESC LIMIT 20 OFFSET $1`
            return client.query(sql, [page*20])
                .then(res => res.rows)
        } else {
            const sql =`select id, username, created_at from guards`
            return client.query(sql)
                 .then(({rows}) => rows.sort((a, b) => a < b ? -1 : 1))
        }
    },

    getByUsername: (username) => {
        const sql = `select * from guards where username = $1`
        return client.query(sql, [username])
            .then(({rows}) => rows[0])
    },

    getById: (id) => {
        const sql = `select * from guards where id = $1`
        return client.query(sql, [id])
            .then(({rows}) => rows[0])
    },

    create: async ({username, hash_password, token}) => {
        try{
			const sql = `insert into Guards(username, password, token)
			values($1, $2, $3)
			returning *`
			return client.query(sql, [username, hash_password, token])
				.then((res) => res.rows[0])
				.then(({id, username, created_at}) => ({id, username, created_at}))
        } catch (e) {
			logger.error(e)
            return e
        }
    },

    update: credentials => {
        const sql_injection = Object.entries(credentials).map(([key, value], index) => `${key}=$${index+1}`).join(', ')
        const sql = `update guards set ${sql_injection} WHERE id=${credentials.id} RETURNING *`
        return client.query(sql, Object.entries(credentials).map(([key, value]) => value))
            .then(res => res.rows[0])
            .then(({id, username, created_at}) => ({id, username, created_at}))
    },

    delete: id => {
        const sql = `delete from guards where id=$1 returning *`
        return client.query(sql, [id])
            .then(({rows}) => {
				if(rows[0]){
					const {id, username, created_at} = rows[0]
					return {id, username, created_at} 
				}
				return null
			})
    },

    checkToken: (token) => {
        const query = `SELECT ($1 IN (SELECT token from guards))`
        return client.query(query, [token]).then(({rows}) => rows[0]["?column?"])
    },

    retrieveToken: (username) => { 
        const query = `select token from guards where username=$1`
        return client.query(query, [username]).then(({rows}) => rows[0])
    },


	getSection: (token) => {
        const query = `select Sections.name from sections join guards on Guards.section=Sections.name where token = $1`
        return client.query(query, [token]).then(({rows}) => {
			if(!rows[0]) return null
			return rows[0].name
		})
	},

    changeSection: ({section_id, token}) => {
        const query = `update guards set section=sections.name 
		from guards as g join sections on sections.id=$1
		where g.token=$2 
		returning g.id, g.username, sections.name as section, g.created_at;`
        return client.query(query, [section_id, token]).then(res => {
			return res.rows[0]
		})
    }
})
