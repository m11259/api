const logWrapper = (type, message) => {
    const time = new Date().toLocaleString()
    const str = '[' + time +  ']' + ' ' + type + ' | '
    return console.log('[' + time +  ']' + ' ' + type + ' ' + message)
}

const statusCodes = code => {
    if (code < 400) {
        return code.toString()
    }
    if (code < 500) {
        return code.toString()
    }
    return code.toString()
}

const methods = {
    'GET': 'GET',
    'POST': 'POST',
    'PUT': 'PUT',
    'DELETE': 'DELETE',
    'PATCH': 'PATCH',
    'WebSocket message': 'WebSocket message',
}

module.exports = {
    error: arg => {
        if(!(arg instanceof Error )) {
            arg = new Error(arg)
        }
       logWrapper('ERROR', arg.name + ': ' + arg.message + '\n' + arg.stack.split('\n').slice(1).join('\n'))
    },
    warn: message => logWrapper('WARN', message),
    info : message => logWrapper('INFO', message),
    write: message => logWrapper('TRACE', message),
    responseInfo: function (method, status, url, adress, port, time){
        this.info(methods[method] + ' | ' + url + ' | ' + statusCodes(status) + ' | ' + 'From: ' +  adress + ':' + port + ' | ' + `${time.miliseconds}ms`)
    }
}
