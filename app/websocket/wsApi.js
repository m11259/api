const db = require('../db/db')
const {prepareRequestsData} = require('../utils/utils_ware')
const {authGuard, authAdmin} = require('../utils/auth_ware')
const logger = require('../logger/logger')

module.exports = {
	'users': (page, token) => authGuard({token}).then(async auth => {
		if(!auth) return 'not auth' 
		if(!(page >= 0)) {
			return `Error, page you send must be postitive integer`
		}
		const section = await db.Guard({getSection: token})
		// const section = null
		page = parseInt(page)
		return await db.Users({getUnidentified: {page, section}}).then(data => {
			return data
		})

	}),

	'requests': (page, token) => authGuard({token}).then(async auth => {
		if(!auth) return 'not auth'
		if(!(page >= 0)) {
			return `Error, page you send must be postitive integer`
		}
		page = parseInt(page)
		const section = await db.Guard({getSection: token})
		// const section = null
		return await db.Requests({get: {page, section}})
			.then(prepareRequestsData)
			.then(reqs => {
				return reqs
			})
	}),
	
	'reqHistory': (page, token) => authGuard({token}).then(async auth => {
		if(!auth) return 'not auth'
		if(page < 0) {
			return `Error, page you send must be postitive integer`
		}
		const section = await db.Guard({getSection: token})
		// const section = null
		page = parseInt(page)
		return await db.Requests({getHistory: {page}})
			.then(prepareRequestsData)
	}),
	
	'lastreqs': (page, token) => authGuard({token}).then(async auth => {
		if(!auth) return 'not auth'	
		const section = await db.Guard({getSection: token})
		// const section = null
		return await db.Requests({getLastAccepted: {number: 5, section}})
			.then(prepareRequestsData)
	}),

	'identsCounter': (_, token) => authAdmin({token}).then(async auth => {
		if(!auth) return 'not auth'
		const res = await db.Users({getIdentsCounter: ''})
		return res
	})

}
