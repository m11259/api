const db = require('../db/db')
const logger = require('../logger/logger')
const {prepareRequestsData} = require('../utils/utils_ware')

const timeDiff = start => ({
    seconds: process.hrtime()[0] - start[0],
    miliseconds: ((process.hrtime()[1] - start[1]) / 1000000).toFixed(2)
})

const listeners = {
	"users/created": {
		events: ['user_created'],
		listener: (id) => db.Users({getById: id}),
		isAdmin: true,
	},
	"users/remove": {
		events: ['user_approved', 'user_denied'],
		listener: (id) => db.Users({getById: id}),
		isAdmin: true,
	},
	"identsCounter": {
		events: ['user_created', 'user_approved', 'user_denied'],
		listener: () => db.Users({getIdentsCounter: ''}),
		isAdmin: true,
	},
	"requests/created": {
		events: ['req_create'],
		listener: (id) => db.Requests({getById: id}),
		isAdmin: false,
	},
	"requests/remove": {
		events: ['req_accept', 'req_deny'],
		listener: (id) => db.Requests({getById: id}),
		isAdmin: false,
	},
    "requests/update": {
        events: ['req_update'],
        listener: (id) => db.Requests({getById: id}),
        isAdmin: false,
    },
	"requests/add": {
		events: ['req_unaccept', 'req_undeny', 'req_created_guard'],
		listener: (id) => db.Requests({getById: id}),
		isAdmin: false,
	},
	"requests/canceled": {
		events: ['req_cancel'],
		listener: req => JSON.parse(req),
		isAdmin: false,
	},
	"lastreqs": {
		events: ['req_unaccept', 'req_undeny', 'req_accept', 'req_deny'],
		listener: (section) => db.Requests({getLastAccepted: {number: 5, section}}),
		getSection: id => db.Requests({getById: id})
			.then(({user_address}) => db.Sections({getByAddress: user_address})),
		isAdmin: false,
	},
	
	"reqHistory/remove": {
		events: ['req_unaccept', 'req_undeny'],
		listener: (id) => db.Requests({getById: id}),
		isAdmin: false,
	},

	"reqHistory/add": {
		events: ['req_accept', 'req_deny'],
		listener: (id) => db.Requests({getById: id}),
		isAdmin: false,
	}
}

module.exports = function(callback) {
	Object.entries(listeners).forEach(([type, {events, listener, isAdmin, getSection}]) => {
		db.on(events, async (id) => {
			const time_start = process.hrtime()
			if(type == "lastreqs"){
				const sections = await getSection(id)
				const data = await listener(sections.length > 0 ? sections[0].name : null)
				// const data = await listener()
				callback({data: prepareRequestsData(data), type}, isAdmin, sections.map(sec => sec.name))
				// callback({data: prepareRequestsData(data), type}, isAdmin)
			}
			else if(['requests', 'reqHistory'].includes(type.split('/')[0])){
				const data = await listener(id)
				const user_address = data.user_address
				const sections = await db.Sections({getByAddress: user_address})
				const prepared_data = prepareRequestsData(data)
				callback({data: prepared_data, type}, isAdmin, sections.map(s => s.name))
				// callback({data: prepared_data, type}, isAdmin)
			} else {
				const data = await listener(id)
				callback({data, type}, isAdmin)
			}
			logger.info(
				`WebSocket message | event '${type}' | 200 | to ${isAdmin ? "Admins" : "Guards"} | ${timeDiff(time_start).miliseconds}ms`
			) 
		})
	})

}

