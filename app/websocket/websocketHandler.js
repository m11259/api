const wsApi = require('./wsApi')
const logger = require('../logger/logger')
module.exports = {
	sessionHandler: (wsSessions, connection, req, message) => {
		message = JSON.parse(message)
		const {data, token} = message
		if(data == "auth"){
			wsSessions.createSession(connection, token)
			logger.info(`Session created for client ${req.socket.remoteAddress}:${req.socket.remotePort}`)
			connection.send(JSON.stringify({ data: `Session created sucessfully`, type: "info"}))
			return true 
		} if(data == "close"){
			wsSessions.closeSession(connection)
			logger.info(`Session closed with client ${req.socket.remoteAddress}:${req.socket.remotePort}`)
			return true
		}
		return false
	},

	sendHandler: (wsSessions) => (message, is_admin, sections) => {
		const clients = wsSessions.getSessions({isGuard: !is_admin, sections})
		clients.forEach(({client}) => {
			client.send(JSON.stringify(message))
		})
	},

	apiHandler: async (connection, message) => {
		message = JSON.parse(message)
		const {data, token} = message
		const [funcname, arg] = data.split('?')
		try {
			const answer = {
				data: await wsApi[funcname](arg, token), 
				type: ['users', 'requests', 'reqHistory'].includes(funcname) ? funcname + "/page" : 
					funcname.split('|') == "Pages" ? funcname + "/pages" : funcname
			}
			connection.send(JSON.stringify(answer))
		} catch(e) {
			logger.error(e)
			connection.send(JSON.stringify({data: `An error occured on server`, type: "info"}))
		}
		return data
	}
}


