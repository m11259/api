const {authGuard, authAdmin} = require('../utils/auth_ware')
const db = require('../db/db')

class WsSessions{
	constructor(ws){
		this.ws = ws
		this.guards = new Set()
		this.admins = new Set()
	}

	async createSession(client, token){
		const isAdmin = await authAdmin({token})
		if(isAdmin){
			this.admins.add({client})
			return 
		}
		const isGuard = await authGuard({token})
		if(isGuard){
			// client.section = await db.Guard({getSection: token})
			this.guards.add({client})
		}
	}

	closeSession(client, token){
		this.admins.delete(client)
		// db.Guard({getSection: token})
		// 	.then(sec => {
		// 		client.section = sec
		// 		this.guards.delete(client)
		// 	})
		this.guards.delete(client)
	}

	getSessions({isGuard, sections}){
		if(isGuard && this.guards.size > 0){
			// if(sections){
			// if(sections.length > 0){
			// 	const guards_from_section = this.guards
			// 	guards_from_section.forEach(g => sections.includes(g.section) ? guards_from_section.delete(g) : '')
			// 	return new Set([...guards_from_section, ...this.admins])
			// }
			// }
			return new Set([...this.guards, ...this.admins])
		}
		return this.admins
	}
}

module.exports = WsSessions
