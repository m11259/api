# MSG Guard Service API

## HTTP API
General request link for http api function: `/api/{endpoint}/{funcname}?{params}`

### Admin
`/api/admin/{funcname}?{params}`

#### Get admin token. Function `/sign`:
Method: `POST`

Body: 
```javascript
{
    username: 'string'
    password: 'string'
}
```

#### Check admin token

Method: `GET`

Headers: `{token}`

### Guards management
`/api/{endpoint}/{funcname}?{params}`

  - `/guards`:
    - `/`: (admin token required)
        - `GET`: `params: page`
        - `POST`: `body: {username, password, [section]}`
        - `PUT`: `body: {id, [username], [password], [section]}`
        - `DELETE`: `params: id`
    - `/sign`:
        - `POST`: `body: {username, password}` - get guard token
        -  `GET`: `headers: {token}` - check guard token
    - `/section`: (guard token required)
        - `PUT`: `params: section_id` - change guard's section
        - `GET` - get guard's section`

### Sections
`/api/{endpoint}/{funcname}?{params}`

- `/sections`: 
    - `/`: (admin token required)
        - `GET`
        - `POST`: `body: {name, from, to}`
        - `PUT`: `body: {id, [name], [flats_range: Array]}`
        - `DELETE`: `params: id`

### Users
- `/users`: 
    - `/`: (admin or guard token required)
        - `GET`: `params: page` - get unidentified users
        - `PUT`: `params: user_id` - identify user
        - `DELETE`: `params: user_id` - deny user
    - `/all`:
        - `GET`: `params: page` - get all user
    - `activation`:
        - `PUT`: `params: user_id` - activate user
        - `PATCH`: `params: user_id` - deactivate user

### Requests
- `requests`:
    - `/`:
        - `PUT`: `params: req_id` - accept request
        - `DELETE`: `params: req_id` - deny request (without delete from db)
        - `POST`: `body: {address, body, car_number}`
    - `file`: 
        - `POST`: `body: {from: Date, to: Date}` - create file with reqs from date to date
    - `fulfill`:
        - `PUT`: `params: req_id` - fulfill request 
    - `setPending`:
        - `PUT`: `body: {id: number, pass:{id: string, type: string}}` - make request pending and add passcard

### WebSocket

General request message for ws api function: 
```javascript
{
    data: '{funcname}?{params}',
    token: 'string'
}
```
#### Functions

##### Retrieve users

Name: `users`

Params: `page`

##### Retrieve requests

Name: `requests`

Params: `page`
